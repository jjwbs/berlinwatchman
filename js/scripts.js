$(function(){
  $("#custom-bootstrap-menu ul li a[href^='#']").on('click', function(e) {
   
   $("#custom-bootstrap-menu ul li").each(function(){
     $(this).removeClass('active');
   });
   
   $(this).parent().addClass('active');
   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 300, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });
  });
});